/*
 * Program used to find the padding used inside structs
 * As sit stands this is only a single experiment
 * @author Chris Coetzee
 */
#include <stdio.h>
#include <stdlib.h>

typedef struct {
    char c[32];
}
test_one;

int main(int argc, char *argv[]) 
{
    test_one a;
    unsigned char *p;
    long int i;
    int size;

    p = &a;
    size = sizeof(test_one);
    
    printf("[SIZE]: %ld \n", size);
   
    /*zero it all out*/ 
    for (i=0;i < size;i++) {
        *(p+i) = 0x00;  
    }
    /*insert values*/
    a.c[0] = 0xff;

    for(i=0;i < size;i++) {
        printf("[BYTE %2d]: %x\n", i, *(p+i));  
    }
}
